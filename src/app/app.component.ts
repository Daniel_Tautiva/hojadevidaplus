import { Component, OnInit } from '@angular/core';

import { Platform, NavController,AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

import { Pages } from './interfaces/pages';
import { UsersService } from './services/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;
  // DEFINIMOS LA VARIABLE DE FORMULARIO
  public onEditForm: FormGroup;
  // DEFINIMOS LA VARIABLE DE LA VISTA
  item={}
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private sqlite: SQLite,
    private userService : UsersService,
    private alertCtrl : AlertController
  ) {
    this.appPages = [
      {
        title: 'Home',
        url: '/home-results',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Acerca de',
        url: '/about',
        direct: 'forward',
        icon: 'information-circle-outline'
      },

      // {
      //   title: 'App Settings',
      //   url: '/settings',
      //   direct: 'forward',
      //   icon: 'cog'
      // }
    ];
    this.initializeApp()
    this.selects_info()
  }

  ionViewWillEnter() {
    this.selects_info()
  }

  // FUNCION QUE LLAMA LOS DATOS DEL USUARIO AUTENTICADO PARA MOSTARLOS EN LA VISTA
  async selects_info(id=localStorage.getItem('user_id')) {
    this.userService.InformationUserCV(id)
    .then(
      (response : any) => {
        alert(JSON.stringify(response))
        if(response.rows.length > 0){
          alert(localStorage.getItem('user_id'))
          this.item = response.rows.item(0);
        }
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    );
  }

  // TOAST DE ERROR
  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Algo ha ocurrido en el servicio :(',
      buttons: ['OK']
    });

    await alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();      
      this.createDatabase()
    }).catch(() => { });
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  createDatabase(){   

    this.sqlite.create({
      name: 'database.db',
      location: 'default'
    })
    .then((db) => {
      this.userService.setDatabase(db);
      return this.userService.createTable();
    })
    .catch(error =>{
      console.error(error);
    }); 
  }

  logout() {
    this.navCtrl.navigateRoot('/');
  }
}
