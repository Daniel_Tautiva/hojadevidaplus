import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  db: SQLiteObject = null;

  constructor() { }

  setDatabase(db: SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }

  checkUser(form: any){    
    let sql = 'SELECT * FROM users where email=? and password=? limit 1';
    return this.db.executeSql(sql, [form.email, form.password]);
  }
  
  createUser(form: any){    
    let sql = 'INSERT INTO users(email, password) VALUES(?,?)';
    return this.db.executeSql(sql, [form.email, form.password]);
  }

  createUserInfo(id: any){
    let sql = 'INSERT INTO cv(user_id) VALUES(?)';
    return this.db.executeSql(sql, [id]);
  }

  InformationUserCV(id: any){    
    let sql = 'SELECT users.email,cv.* FROM users JOIN cv ON(users.id=cv.user_id) where cv.user_id=? limit 1';
    return this.db.executeSql(sql, [id]);
  }
  
  UpdateProfile(form: any,id:any){
    let sql = 'UPDATE cv SET names=?, address=?, country=?, city=? WHERE id=?';
    return this.db.executeSql(sql, [form.full_name, form.address, form.country,form.city,id]);
  }

  UpdateCV(form: any,id:any){
    let sql = 'UPDATE cv SET names=?, last_names=?,birthdate=?,civil_status=?,type_identification=?,number_identification=?,address=?,country=?,city=?,position=?,description_position=? WHERE id=?';     
    return this.db.executeSql(sql, [form.full_name, '' , form.birthdate, form.civil_state, form.type_identification, form.number_identification,form.address,form.country,form.city,form.position,form.description_position,id]);
  }


  createTable(){

    let usersTable = 'CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT NOT NULL UNIQUE, password TEXT)';
    this.db.executeSql(usersTable, []);
    
    let CVTable = 'CREATE TABLE IF NOT EXISTS cv(id INTEGER PRIMARY KEY AUTOINCREMENT,user_id INTEGER, names TEXT, last_names TEXT,birthdate DATE,civil_status INTEGER,type_identification INTEGER,number_identification VARCHAR(20),address VARCHAR(50),country VARCHAR(50),city VARCHAR(50),position VARCHAR(50),description_position TEXT)';
    return this.db.executeSql(CVTable, []);

  }

  update(task: any){
    let sql = 'UPDATE tasks SET title=?, completed=? WHERE id=?';
    return this.db.executeSql(sql, [task.title, task.completed, task.id]);
  }

}
