import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public userService: UsersService
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  // DEFINIMOS UN ARRAY PARA GUARDAR INFO DEL USUARIO
  results=[]

  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }

  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Datos incorrectos.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async show_data(data) {
    const alert = await this.alertCtrl.create({
      header: 'data',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }

  async login() {

    var credentials = this.onLoginForm.value;

    const loader = await this.loadingCtrl.create({
      duration: 500
    });
    loader.present();

    this.userService.checkUser(credentials)
    .then(
      (res : any) => {

        // this.show_data(JSON.stringify(res))

        if(res.rows.length > 0){
          
          // ASIGNAMOS A LA VARIABLE ITEM LOS VALORES DEVUELTOS POR LA CONSULTA
          let item = res.rows.item(0);
          // CREAMOS LA VARIABLE QUE ALMACENA EL ID DE USUARIO Y ASIGNAMOS EL VALOR
          let user_id=item.id;
          // ALMACENAMOS LA VARIABLE EN EL LOCALSTORAGE PARA NO ESTAR CONSULTANDO CADA RATO
          localStorage.setItem('user_id', user_id.toString());

          // this.show_data(JSON.stringify(user_id))
          this.navCtrl.navigateRoot('/home-results');
        }
        else{
          this.error()
        }
      }
    )
    .catch(
      (err : any) => {
        console.log(err)

        this.error()
        
      }
    );   
  }

}
