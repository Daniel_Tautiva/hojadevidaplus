import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  LoadingController,
  ModalController } from '@ionic/angular';

// Modals
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { UsersService } from 'src/app/services/users.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Profile } from 'selenium-webdriver/firefox';


@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage implements OnInit{

  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/ucc.jpg';
  // DEFINIMOS LA VARIABLE DE FORMULARIO
  public onEditForm: FormGroup;
  // DEFINIMOS LA VARIABLE DE LA VISTA
  item={}

  profileProInputs: any;
  profileEduInputs: any;
  languageInputs: any;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public userService: UsersService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,

  ) {     this.select_info();     }

  ngOnInit(){

    this.onEditForm = this.formBuilder.group({
      'full_name': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null],
      'birthdate': [null, Validators.compose([
        Validators.required
      ])],
      'type_identification': [null, Validators.compose([
        Validators.required
      ])],
      'number_identification': [null, Validators.compose([
        Validators.required
      ])],
      'civil_state': [null, Validators.compose([
        Validators.required
      ])],
      'position': [null, Validators.compose([
        Validators.required
      ])],
      'description_position': [null, Validators.compose([
        Validators.required
      ])],
      'address': [null, Validators.compose([
        Validators.required
      ])],
      'country': [null, Validators.compose([
        Validators.required
      ])],
      'city': [null, Validators.compose([
        Validators.required
      ])],
      'profile_pro': this.formBuilder.array([]),
      'profile_edu': this.formBuilder.array([]),
      'language': this.formBuilder.array([])

    });

    let infHV: any = JSON.parse(localStorage.getItem('Hv'));

    

    // for(var key in infHV){
    //   if(this.onEditForm.controls[key]){
    //     this.onEditForm.controls[key].setValue(infHV[key]);
    //   }
    // }

    
    for(var indice in infHV['profile_pro']){
      this.addPerfilePro();
      for(var key in infHV['profile_pro'][indice]){
        if(this.profileProInputs.controls[indice].controls[key]){
          this.profileProInputs.controls[indice].controls[key].setValue(infHV['profile_pro'][indice][key]);
        }

      }
    }

    for(var indice2 in infHV['profile_edu']){
      this.addPerfileEdu();
      for(var key in infHV['profile_edu'][indice2]){
        if(this.profileEduInputs.controls[indice2].controls[key]){
          this.profileEduInputs.controls[indice2].controls[key].setValue(infHV['profile_edu'][indice2][key]);
        }

      }
    }

    for(var indice3 in infHV['language']){
      this.addLanguage();
      for(var key in infHV['language'][indice3]){
        if(this.languageInputs.controls[indice3].controls[key]){
          this.languageInputs.controls[indice3].controls[key].setValue(infHV['language'][indice3][key]);
        }

      }
    }

  }

  profile_pro(){
    return this.onEditForm.get('profile_pro') as FormArray;
  }

  addPerfilePro(){

    const profileProFormGroup = this.formBuilder.group({

      'business': [null, Validators.compose([
        Validators.required
      ])],
      'company_sector': [null, Validators.compose([
        Validators.required
      ])],
      'position': [null, Validators.compose([
        Validators.required
      ])],
      'area': [null, Validators.compose([
        Validators.required
      ])],
      'functions': [null, Validators.compose([
        Validators.required
      ])]

    });

    this.profile_pro().push(profileProFormGroup);
    this.profileProInputs = this.profile_pro();

  }

  removePerfilePro(indice:number){
    this.profileProInputs.removeAt(indice);
  } 

  profile_edu(){
    return this.onEditForm.get('profile_edu') as FormArray;
  }

  addPerfileEdu(){

    const profileEduFormGroup = this.formBuilder.group({

      'educational_center': [null, Validators.compose([
        Validators.required
      ])],
      'level_of_studies': [null, Validators.compose([
        //Validators.required
      ])],
      'state_education': [null, Validators.compose([
        Validators.required
      ])]
    });
    
    this.profile_edu().push(profileEduFormGroup);
    this.profileEduInputs = this.profile_edu();

  }

  removePerfileEdu(indice:number){
    this.profileEduInputs.removeAt(indice)
  } 

  languageInput(){
    return this.onEditForm.get('language') as FormArray;
  }

  addLanguage(){

    const languageFormGroup = this.formBuilder.group({

      'idioma': [null, Validators.compose([
        Validators.required
      ])],
      'nivel': [null, Validators.compose([
        Validators.required
      ])]

    });
    
    this.languageInput().push(languageFormGroup);
    this.languageInputs = this.languageInput();

  }

  removeLanguage(indice){
    this.languageInputs.removeAt(indice);
  } 

  ionViewWillEnter() {
    this.select_info()
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  // FUNCION QUE LLAMA LOS DATOS DEL USUARIO AUTENTICADO PARA MOSTARLOS EN LA VISTA
  async select_info(id=localStorage.getItem('user_id')) {
    this.userService.InformationUserCV(id)
    .then(
      (response : any) => {
        if(response.rows.length > 0){
          this.item = response.rows.item(0);
        }
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    );
  }

  // FUNCION QUE ACTUALIZA LOS DATOS
  async update_data(){
    var information = this.onEditForm.value;
    
    this.userService.UpdateCV(information,localStorage.getItem('user_id'))
    .then(
      (res : any) => {
        localStorage.setItem('Hv',JSON.stringify(this.onEditForm.value));
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    ); 
  }

  async save_data(){
    // LLamamos la funcion que actualiza los datos
    this.update_data()

    const loader = await this.loadingCtrl.create({
      duration: 500
    });
    loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        // cssClass: 'bg-profile',
        message: 'Hoja de vida Actualizada!',
        duration: 1000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('home-results');
    });
  }

  // TOAST DE ERROR
  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Algo ha ocurrido en el servicio :(',
      buttons: ['OK']
    });

    await alert.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

}
