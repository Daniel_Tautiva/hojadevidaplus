import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController,AlertController } from '@ionic/angular';
import { UsersService } from 'src/app/services/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  
  // DEFINIMOS LA VARIABLE DE FORMULARIO
  public onEditForm: FormGroup;
  // DEFINIMOS LA VARIABLE DE LA VISTA
  item={}

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public userService: UsersService,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    ) { }


  ngOnInit() {

    this.select_info()
    this.onEditForm = this.formBuilder.group({
      'full_name': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null],
      'address': [null, Validators.compose([
        Validators.required
      ])],
      'country': [null, Validators.compose([
        Validators.required
      ])],
      'city': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  // FUNCION QUE LLAMA LOS DATOS DEL USUARIO AUTENTICADO PARA MOSTARLOS EN LA VISTA
  async select_info(id=localStorage.getItem('user_id')) {
    this.userService.InformationUserCV(id)
    .then(
      (response : any) => {

        if(response.rows.length > 0){
          this.item = response.rows.item(0);
        }

      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    );
  }

  // FUNCION QUE ACTUALIZA LOS DATOS
  async update_data(){
    var information = this.onEditForm.value;

    this.userService.UpdateProfile(information,localStorage.getItem('user_id'))
    .then(
      (res : any) => {
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    ); 
  }

  async save_data(){

    // LLamamos la funcion que actualiza los datos
    this.update_data()

    const loader = await this.loadingCtrl.create({
      duration: 500
    });
    loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        // cssClass: 'bg-profile',
        message: 'Datos Guardados!',
        duration: 1000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('home-results');
    });
  }




  // TOAST DE ERROR
  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Algo ha ocurrido en el servicio :(',
      buttons: ['OK']
    });

    await alert.present();
  }




  // *******************ESTA FUNCION VIENE CON EL TEMPLATE*****
  async sendData() {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'bg-profile',
        message: 'Your Data was Edited!',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('home-results');
    });
  }
  // *******************************************************

}
